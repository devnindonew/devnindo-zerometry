import 'package:flutter/material.dart';
import 'package:Zerometry/attendance/attendanceReview.dart';
import 'package:Zerometry/leaveManagement/riseOvertime.dart';
import 'package:Zerometry/leaveManagement/takeLeave.dart';
import 'package:Zerometry/navigation.dart';
import 'package:Zerometry/passRecovery.dart';
import 'package:Zerometry/profile/Profile.dart';
import 'package:Zerometry/catering/catering.dart';
import 'package:Zerometry/profile/editProfile.dart';
import 'package:Zerometry/catering/mealOrder.dart';
import 'package:Zerometry/profile/resetPassword.dart';
import 'logIn.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/recoveryPass': (BuildContext context) => RecoveryPass(),
        '/logIn': (BuildContext context) => LogInPage(),
        '/navigationPage': (BuildContext context) => NavigationPage(),
        '/mealOrder': (BuildContext context) => MealOrder(),
        '/catering': (BuildContext context) => Catering(),
        '/profile': (BuildContext context) => Profile(),
        '/resetPass': (BuildContext context) => ResetPassword(),
        '/editProfile': (BuildContext context) => EditProfile(),
        '/takeLeave': (BuildContext context) => TakeLeave(),
        '/riseOvertime': (BuildContext context) => RiseOvertime(),
        '/reviewAttendance': (BuildContext context) => AttendanceReview(),
        
      },
      home: NavigationPage(),
      theme: new ThemeData(
        primarySwatch: Colors.green,
      ),
    );
  }
}

